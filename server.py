from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import subprocess

hostName = "0.0.0.0"
serverPort = 8080

def do_task(percentage, time):
    subprocess.Popen('stress-ng -c 0 -l ' + percentage + '--timeout ' + time, shell=True)

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        parts = self.path.split("/")
        if ( len(parts) == 3):
            do_task(parts[1], parts[2])
            self.wfile.write(bytes("Spawning task for " + parts[1] + " " + parts[2], "utf-8"))
        else:
            self.wfile.write(bytes("Invalid :(", "utf-8"))


if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
