import subprocess
import threading
import requests
import time
import multiprocessing
from flask import Flask, request

HIGH_LOAD_MAX = 16
LOW_LOAD_MAX = 2
TIMEOUT = 15

app = Flask(__name__)
clients = ["192.168.122.18"]
processes = []
is_running = False

def alert_client(ip, pct, time):
    print("Sending job to client " + ip)
    requests.get(f'http://{ip}:8080/{pct}/{time}')

def worker_thread(max_l):
    while True:
        global is_running
        if is_running:
            print("Respawning work jobs")
            per_client = max_l//len(clients)
            for client in clients:
                for i in range(per_client):
                    alert_client(client, 10, TIMEOUT)
            time.sleep(TIMEOUT)


@app.route('/client', methods=['GET'])
def list_clients():
    return "\n".join(clients)

@app.route('/client/add', methods=['GET'])
def add_client():
    clients.append(request.args['ip'])
    return "Added client to list of clients"

@app.route('/client/remove', methods=['GET'])
def remove_client():
    clients.remove(request.args['ip']) if '' in request.args['ip'] else None
    return "Removed client"

@app.route('/start', methods=['GET'])
def start_load():
    type = request.args['type']
    max = LOW_LOAD_MAX
    if ( type == 'high'):
        max = HIGH_LOAD_MAX
    global is_running
    is_running= True
    t1 = threading.Thread(target = worker_thread, args=(max,))
    t1.start()
    return "Starting load"

@app.route("/stop", methods=['GET'])
def stop_load():
    global is_running
    is_running = False
    return "Stopping load"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1234)
