# get Active domain => spawn thread to watch CPU usage
# if cpu usage above threshold => spawn VM2
# => alert client
import libvirt
import time
import requests

VMS = [
        { 'name': 'cs4037d_1', 'ip': '192.168.122.18'},
        { 'name': 'cs4037d_2', 'ip':'192.168.122.74'}
]

CLIENT_URL = "http://localhost:1234/client/add"

def start_vm(conn, vm):
    domain = conn.lookupByName(vm['name'])
    print("Starting new VM")
    if ( domain.isActive()):
        return
    domain.create()
    while True:
        try:
            res = requests.get(f"http://{vm['ip']}:8080")
            print("Checking status")
            if ( res.status_code == 200):
                break
        except:
            pass

    try:
        requests.get(f"{CLIENT_URL}?ip={vm['ip']}")
        print("Client informed of new VM")
    except:
        print("Informing client failed")



TIME_INTERVAL = 10

conn = libvirt.open("qemu:///system")

domain = conn.lookupByName(VMS[0]['name'])
while True:
    t1 = time.time()
    c1 = int(domain.info()[4])
    time.sleep(TIME_INTERVAL);
    t2 = time.time();
    c2 = int(domain.info()[4])
    c_nums = int(domain.info()[3])
    usage = (c2-c1)*100/((t2-t1)*c_nums*1e9)
    if ( usage > 45):
        start_vm(conn, VMS[1])
    print("\r%s Cpu usage %f" % (domain.name(),usage), end="\r\n")

conn.close()
